package upou;

/**
 *
 * @author Jino Raymundo
 */
public enum TokenType {
    COMMENT, ASSIGN,
    IDENTIFIER, DIV_SEG,
    DELIMITER, D_TYPE, PROG_END,
    // MANEUVER
    VAR_INPUT, PRINT,
    VARIABLE,
    STRING,
    INVALID,
    // Numeric Operations
    NUM_OPR,
    // Numeric Predicates
    NUM_PRE,
    // Logical Operations
    LOG_OPS,
    BOOL,
    VAR_NAME,
    FLOAT
    ;
    
    private String text;
    
    TokenType(){
        this.text = this.toString();
    }
    
    TokenType(String text){
        this.text = text;
    }
    
    public String getText(){
        return text;
    }
}
