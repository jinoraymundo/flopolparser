package upou;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 *
 * @author Jino Raymundo
 */
public class SmallParser {
    static String nextToken = "";
    static String code = "";
    static int nextIndex = 0;
    static int tokenCount = 0;
    static boolean containsInvalidTokens; 
    static boolean containsParseError;
    static boolean containsBegVariables;
    
    static ArrayList<Token> tokens;
    static ArrayList<Variable> variables;
    static ArrayList<String> output;
    static ArrayList<String> begVariables;
    
    static String parseErrorLocation;    
    
    public SmallParser(String input){
        code = input;
        nextIndex = 0;
        tokenCount = 0;
        tokens = new ArrayList<Token>();
        variables = new ArrayList<Variable>();
        output = new ArrayList<String>();
        begVariables = new ArrayList<String>();
        containsInvalidTokens = false;
        containsParseError = false;
        containsBegVariables = false;        
        try{            
            lexer(code);            
        }catch(Exception e){
            
        }
        parser();
    }        
    
    public static void parser(){        
        
        int i = 1; // parser index
        
        // check if code is enclosed within FLOP
        if(tokens.get(0).getType() == TokenType.DELIMITER && tokens.get(tokens.size() - 1).getType() == TokenType.DELIMITER){                                                                        
            
            // CREATE DIVISION
            if(tokens.get(i).getType() == TokenType.DIV_SEG){

                i++;

                // loop through declarations
                while(tokens.get(i).getType() == TokenType.D_TYPE || tokens.get(i).getType() == TokenType.COMMENT){

                    if(tokens.get(i).getType() == TokenType.D_TYPE){

                        i++;

                        if(tokens.get(i).getType() == TokenType.VAR_NAME){

                            i++;

                            if(tokens.get(i).getType() == TokenType.ASSIGN){

                                i++;

                                // variable is directly assigned to value
                                if(tokens.get(i).getType() == TokenType.FLOAT || 
                                   tokens.get(i).getType() == TokenType.BOOL ||
                                   tokens.get(i).getType() == TokenType.STRING){                                   

                                    evaluateAssignment(i);
                                    i++;

                                // variable is assigned to a numerical operation
                                } else if(tokens.get(i).getType() == TokenType.NUM_OPR){
                                    
                                    i++;
                                
                                    // if variable is assigned to 2 variables
                                    if(tokens.get(i).getType() == TokenType.VAR_NAME){
                                        
                                        i++;
                                        
                                        if(tokens.get(i).getType() == TokenType.VAR_NAME){
                                        
                                            evaluateNumOperation(i);
                                            i++;
                                            
                                        }
                                        
                                    // if assignment to 2 float values
                                    } else if(tokens.get(i).getType() == TokenType.FLOAT){
                                        
                                        i++;
                                        
                                        if(tokens.get(i).getType() == TokenType.FLOAT){
                                            
                                            evaluateNumOperationValues(i);
                                            i++;
                                        }
                                        
                                    }
                                    
                                } else if(tokens.get(i).getType() == TokenType.NUM_PRE){
                                    
                                    i++;
                                    
                                    // if variable is assigned to 2 variables
                                    if(tokens.get(i).getType() == TokenType.VAR_NAME){
                                        
                                        i++;
                                        
                                        if(tokens.get(i).getType() == TokenType.VAR_NAME){
                                        
                                            evaluateNumPredicate(i);
                                            i++;
                                            
                                        }
                                        
                                    // if assignment to 2 float values
                                    } else if(tokens.get(i).getType() == TokenType.FLOAT){
                                        
                                        i++;
                                        
                                        if(tokens.get(i).getType() == TokenType.FLOAT){
                                            
                                            evaluateNumPredicateValues(i);
                                            i++;
                                        }
                                        
                                    }
                                
                                } else if(tokens.get(i).getType() == TokenType.LOG_OPS){
                                                                    
                                    i++;
                                    
                                    if(tokens.get(i).getType() == TokenType.BOOL){
                                        
                                        i++;
                                        
                                        if(tokens.get(i).getType() == TokenType.BOOL){
                                            
                                            evaluateLogOps(i);
                                            i++;
                                            
                                        } else parseError("INVALID DATA TYPE");
                                        
                                    } else parseError("INVALID DATA TYPE");
                                    
                                }else parseError("DATA TYPE TO VALUE");

                            } else parseError("ASSIGN");

                        } else parseError("VAR_NAME");

                    } else if(tokens.get(i).getType() == TokenType.COMMENT){
                    
                        i++;
                        
                    }else parseError("D_TYPE");

                } // end loop for declarations                                        

            } else parseError("DIVISION");                                
                        
            
            if(tokens.get(i).getType() == TokenType.PROG_END){ 
                        
                i++;

            } else parseError("PROG_END");  
            
            
            // MANEUVER DIVISION
            if(tokens.get(i).getType() == TokenType.DIV_SEG){
                
                i++;
                
                // loop print/operation statements
                while(tokens.get(i).getType() == TokenType.PRINT || tokens.get(i).getType() == TokenType.VAR_INPUT){
                    
                    if(tokens.get(i).getType() == TokenType.PRINT){

                        i++;                                            
                        if(tokens.get(i).getType() == TokenType.STRING ||
                           tokens.get(i).getType() == TokenType.BOOL ||
                           tokens.get(i).getType() == TokenType.FLOAT){

                            buildOutput(i);
                            i++;                            

                        } else if(tokens.get(i).getType() == TokenType.VAR_NAME){
                            
                            buildOutput(i, tokens.get(i).getText());
                            i++;
                            
                        } // else part


                    } else if(tokens.get(i).getType() == TokenType.VAR_INPUT){
                        
                        i++;
                        
                        if(tokens.get(i).getType() == TokenType.VAR_NAME){
                            begVariables.add(tokens.get(i).getText());
                            i++;
                        }
                        
                    } // else part to be written
                    
                }                                
                
            } else { 
                parseError("DIV_SEG");                
            }
            
        } else parseError("DELIMITER");
            
    }        
    
    public static void evaluateNumPredicateValues(int i){
        
        String numOpr = tokens.get(i - 2).getText();
        String op1 = tokens.get(i - 1).getText();
        String op2 = tokens.get(i).getText();                
        
        String name = tokens.get(i - 4).getText();
        String type = tokens.get(i - 5).getText();
        
        if(!numOpr.equals("PING")){
            
            for(Variable v : variables){
                if(op1.equals(v.getName())){
                    op1 = v.getValue();                        
                }                    
            }                                

            for(Variable v : variables){
                if(op2.equals(v.getName())){
                    op2 = v.getValue();                        
                }                    
            }

            if(op1.equals("") || op2.equals(""))
                parseError("INCOMPATIBLE TYPES");
            
            if(numOpr.equals("GT?")){                                                                    
                String result = (Float.parseFloat(op1) > Float.parseFloat(op2)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("GTE?")){
                String result = (Float.parseFloat(op1) >= Float.parseFloat(op2)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("LT?")){
                String result = (Float.parseFloat(op1) < Float.parseFloat(op2)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("LTE?")){
                String result = (Float.parseFloat(op1) <= Float.parseFloat(op2)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("EQ?")){                
                String result = (Float.parseFloat(op1) == Float.parseFloat(op2)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("NEQ?")){                
                String result = (Float.parseFloat(op1) != Float.parseFloat(op2)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            }
            
        } else {
                        
        }
        
    }
    
    public static void evaluateNumOperationValues(int i){
        
        String numOpr = tokens.get(i - 2).getText();
        String op1 = tokens.get(i - 1).getText();
        String op2 = tokens.get(i).getText();                
        
        String name = tokens.get(i - 4).getText();
        String type = tokens.get(i - 5).getText();
        
        if(!numOpr.equals("PING")){
            if(tokens.get(i - 1).getType() != TokenType.FLOAT || tokens.get(i).getType() != TokenType.FLOAT)
                parseError("INCOMPATIBLE TYPES");
            
            if(numOpr.equals("ADD")){                                                
                String sum = Float.parseFloat(op1)+Float.parseFloat(op2) + "";                
                variables.add(new Variable(name, type, (sum.substring(sum.indexOf(".")).length() == 3) ? sum += '0' : sum));                                    
            } else if(numOpr.equals("SUB")){
                String diff = Float.parseFloat(op1)-Float.parseFloat(op2) + "";                
                variables.add(new Variable(name, type, (diff.substring(diff.indexOf(".")).length() == 2) ? diff += '0' : diff));
            } else if(numOpr.equals("MULT")){
                String prod = Float.parseFloat(op1)*Float.parseFloat(op2) + "";                
                variables.add(new Variable(name, type, (prod.substring(prod.indexOf(".")).length() == 2) ? prod += '0' : prod));
            } else if(numOpr.equals("DIVI")){
                String quot = Float.parseFloat(op1)/Float.parseFloat(op2) + "";                
                variables.add(new Variable(name, type, (quot.substring(quot.indexOf(".")).length() == 2) ? quot += '0' : quot));
            } else if(numOpr.equals("EXP")){                
                String expResult = Math.pow(Float.parseFloat(op1), Float.parseFloat(op2)) + "";
                variables.add(new Variable(name, type, (expResult.substring(expResult.indexOf(".")).length() == 2) ? expResult += '0' : expResult));
            }
        }
    }
    
    public static void evaluateNumPredicate(int i){
        
        String numOpr = tokens.get(i - 2).getText();
        String op1 = tokens.get(i - 1).getText();
        String op2 = tokens.get(i).getText();
        
        String op1Val = "";
        String op2Val = "";
        
        String name = tokens.get(i - 4).getText();
        String type = tokens.get(i - 5).getText();
        
        if(!numOpr.equals("PING")){
            
            for(Variable v : variables){                
                if(op1.equals(v.getName())){
                    op1Val = v.getValue();                        
                }                    
            }                                

            for(Variable v : variables){
                if(op2.equals(v.getName())){
                    op2Val = v.getValue();                        
                }                    
            }

            if(op1Val.equals("") || op2Val.equals(""))
                parseError("INCOMPATIBLE TYPES");
            
            if(numOpr.equals("GT?")){                                                                    
                String result = (Float.parseFloat(op1Val) > Float.parseFloat(op2Val)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("GTE?")){
                String result = (Float.parseFloat(op1Val) >= Float.parseFloat(op2Val)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("LT?")){
                String result = (Float.parseFloat(op1Val) < Float.parseFloat(op2Val)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("LTE?")){
                String result = (Float.parseFloat(op1Val) <= Float.parseFloat(op2Val)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("EQ?")){                
                String result = (Float.parseFloat(op1Val) == Float.parseFloat(op2Val)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            } else if(numOpr.equals("NEQ?")){                
                String result = (Float.parseFloat(op1Val) != Float.parseFloat(op2Val)) ? "YEAH" : "NOPE";
                variables.add(new Variable(name, type, result));                                    
            }
            
        } else {
                        
        }
        
    }
    
    public static void evaluateLogOps(int i){
        
        String logOps = tokens.get(i - 2).getText();
        String op1 = tokens.get(i - 1).getText();
        String op2 = tokens.get(i).getText();
        
        String name = tokens.get(i - 4).getText();
        String type = tokens.get(i - 5).getText();
        
        if(logOps.equals("INTER")){
            String result = (op1.equals("YEAH") && op2.equals("YEAH")) + "";
            variables.add(new Variable(name, type, (result.equals("true")) ? "YEAH" : "NOPE"));                                    
        } else if(logOps.equals("UNION")){
            String result = (op1.equals("YEAH") || op2.equals("YEAH")) + "";
            variables.add(new Variable(name, type, (result.equals("true")) ? "YEAH" : "NOPE"));                                    
        } 
        
    }
    
    public static void evaluateNumOperation(int i){
        
        String numOpr = tokens.get(i - 2).getText();
        String op1 = tokens.get(i - 1).getText();
        String op2 = tokens.get(i).getText();
        
        String op1Val = "";
        String op2Val = "";
        
        String name = tokens.get(i - 4).getText();
        String type = tokens.get(i - 5).getText();
        
        if(!numOpr.equals("PING")){
            
            for(Variable v : variables){
                if(op1.equals(v.getName()) && v.getType().toString().equals(type)){
                    op1Val = v.getValue();                        
                }                    
            }                                

            for(Variable v : variables){
                if(op2.equals(v.getName()) && v.getType().toString().equals(type)){
                    op2Val = v.getValue();                        
                }                    
            }

            if(op1Val.equals("") || op2Val.equals(""))
                parseError("INCOMPATIBLE TYPES");
            
            if(numOpr.equals("ADD")){                                                
                String sum = Float.parseFloat(op1Val)+Float.parseFloat(op2Val) + "";                
                variables.add(new Variable(name, type, (sum.substring(sum.indexOf(".")).length() == 3) ? sum += '0' : sum));                                    
            } else if(numOpr.equals("SUB")){
                String diff = Float.parseFloat(op1Val)-Float.parseFloat(op2Val) + "";                
                variables.add(new Variable(name, type, (diff.substring(diff.indexOf(".")).length() == 2) ? diff += '0' : diff));
            } else if(numOpr.equals("MULT")){
                String prod = Float.parseFloat(op1Val)*Float.parseFloat(op2Val) + "";                
                variables.add(new Variable(name, type, (prod.substring(prod.indexOf(".")).length() == 2) ? prod += '0' : prod));
            } else if(numOpr.equals("DIVI")){
                String quot = Float.parseFloat(op1Val)/Float.parseFloat(op2Val) + "";                
                variables.add(new Variable(name, type, (quot.substring(quot.indexOf(".")).length() == 2) ? quot += '0' : quot));
            } else if(numOpr.equals("EXP")){                
                String expResult = Math.pow(Float.parseFloat(op1Val), Float.parseFloat(op2Val)) + "";
                variables.add(new Variable(name, type, (expResult.substring(expResult.indexOf(".")).length() == 2) ? expResult += '0' : expResult));
            }
            
        } else {
                        
        }
        
    }
    
    public static void buildOutput(int i){
        String printType = tokens.get(i - 1).getText();
        String stringToPrint = tokens.get(i).getText().replace("$", "");        
        
        if(printType.equals("GIVEMORE"))
            output.add(stringToPrint + '\n');
        else output.add(stringToPrint);
    }
    
    public static void buildOutput(int i, String varName){
        String printType = tokens.get(i - 1).getText();
        String varValue = "";     
         System.out.println(varName);
        for(Variable v : variables){            
            if(v.getName().equals(varName)){                
                varValue = v.getValue();                    
                System.out.println(varValue + " " + v.getValue() + " " + varName);                
                break;                
            }
        }                        
        
        if(!varValue.equals("")){
            if(printType.equals("GIVEMORE"))
                output.add(varValue + '\n');
            else output.add(varValue);
        } else parseError("UNDEFINED VARIABLE");
            
    }
    
    public static void evaluateAssignment(int i){
        String name = tokens.get(i - 2).getText();
        String type = tokens.get(i - 3).getText();
        String value = tokens.get(i).getText();
        
        if(type.equals(tokens.get(i).getType().toString())){
            Variable v = new Variable(name, type, value);
            variables.add(v);
        } else { 
            parseError("TYPE TO VALUE MISMATCH");            
        }
                
    }
    
    public static void parseError(String position){
        containsParseError = true;
        parseErrorLocation = position;        
    }
    
    public static void lexer(String code) throws Exception{
        
        while(nextIndex < code.length()){
            nextToken = "";
            char character = code.charAt(nextIndex);
            
            while(character == ' ' || character == '\t' || character == '\r' || character == '\n'){
                nextIndex++;
                character = code.charAt(nextIndex);
            }
            
            if(Character.isUpperCase(character)){
                matchIdentifier();
                nextIndex++;
                nextToken = nextToken + code.charAt(nextIndex);
            } else if(Character.isLowerCase(character)){
                matchVariable();
                nextIndex++;
                nextToken = nextToken + code.charAt(nextIndex);
            } else if(Character.isDigit(character)){
                matchFloat();
                nextIndex++;
                nextToken = nextToken + code.charAt(nextIndex);
            } else if(character == '$') {
                matchStringLiteral();
                nextIndex++;
                nextToken = nextToken + code.charAt(nextIndex);
            } else if(character == '*'){
                matchComment();
                nextIndex++;
                nextToken = nextToken + code.charAt(nextIndex);
            } else {
                nextIndex++;
                character = code.charAt(nextIndex);
            }
                        
            
        } // end while        
    }
    
    private static void matchFloat(){
        StringBuilder sb = new StringBuilder();
        int character = code.charAt(nextIndex);                
        
        while((character >= '0' && character <= '9') || character == '.'){
            sb.append((char)character);
            
            if(nextIndex == code.length() - 1 || code.charAt(nextIndex) == ' ')
                break;
            else character = code.charAt(++nextIndex);
            
        }
        
        String word = sb.toString();
        
        int indexOfDot = word.indexOf('.');
        char firstDigit = word.charAt(indexOfDot + 1);
        char secondDigit = word.charAt(indexOfDot + 2);
        
        if((indexOfDot == -1) || (!Character.isDigit(firstDigit) || !Character.isDigit(secondDigit))){
            tokens.add(new Token(TokenType.INVALID, word));
            containsInvalidTokens = true;
        } else {
            tokens.add(new Token(TokenType.FLOAT, word));
        }
    }
    
    private static void matchVariable(){
        StringBuilder sb = new StringBuilder();
        int character = code.charAt(nextIndex);                
        
        while((character >= 'a' && character <= 'z') || (character >= '0' || character <= '9')){            
            sb.append((char)character);    
            
            if(nextIndex == code.length() - 1 || code.charAt(nextIndex) == ' ')
                break;
            else
                character = code.charAt(++nextIndex);            
        }                
        
        String word = sb.toString().trim();                
        
        if(Character.isDigit(word.charAt(0))){
            tokens.add(new Token(TokenType.INVALID, word));
            containsInvalidTokens = true;
        }
        else {
            tokens.add(new Token(TokenType.VAR_NAME, word));
        }
    }
    
    private static void matchComment(){
        StringBuilder sb = new StringBuilder();
        int character = code.charAt(nextIndex);                
        
        while(character == '*' || (character >= 'A' && character <= 'z') || character == ' '){
            sb.append((char)character);
            if(nextIndex == code.length() - 1)
                break;
            else 
                character = code.charAt(++nextIndex);
        }                
        
        String word = sb.toString();
        if(word.startsWith("*") && word.endsWith("*"))
           tokens.add(new Token(TokenType.COMMENT, word));
        else {
            tokens.add(new Token(TokenType.INVALID, word));  
            containsInvalidTokens = true;
        }
    }
    
    private static void matchStringLiteral(){
        StringBuilder sb = new StringBuilder();
        int character = code.charAt(nextIndex);                
        
        while(character == '$' || (character >= 'A' && character <= 'z') || character == ' '){
            sb.append((char)character);
            if(nextIndex == code.length() - 1)
                break;
            else 
                character = code.charAt(++nextIndex);
        }
                
        
        String word = sb.toString();
        if(word.startsWith("$") && word.endsWith("$"))
           tokens.add(new Token(TokenType.STRING, word));
        else {
            tokens.add(new Token(TokenType.INVALID, word));  
            containsInvalidTokens = true;
        }
    }
    
    private static void matchIdentifier(){
        StringBuilder sb = new StringBuilder();
        int character = code.charAt(nextIndex);
        
        while((character >= 'A' && character <= 'z') || character == '?'){            
            sb.append((char)character);    
            
            if(nextIndex == code.length() - 1)
                break;
            else
                character = code.charAt(++nextIndex);            
        }                
        
        String word = sb.toString();
        
        if(word.equals("FLOP")){
            tokens.add(new Token(TokenType.DELIMITER, word));
        } else if(word.equals("CREATE") || word.equals("MANEUVER")){
            tokens.add(new Token(TokenType.DIV_SEG, word));
        } else if(word.equals("END")){
            tokens.add(new Token(TokenType.PROG_END, word));
        } else if(word.equals("BEG")){
            tokens.add(new Token(TokenType.VAR_INPUT, word));
            containsBegVariables = true;            
        } else if(word.equals("GIVE") || word.equals("GIVEMORE")){
            tokens.add(new Token(TokenType.PRINT, word));
        } else if(word.equals("ADD") || word.equals("SUB") || word.equals("MULT") ||
                word.equals("DIVI") || word.equals("PING") || word.equals("EXP") ||
                word.equals("REMI")) {
            tokens.add(new Token(TokenType.NUM_OPR, word));
        } else if(word.equals("GT?") || word.equals("GTE?") || word.equals("LT?") ||
                word.equals("LTE?") || word.equals("EQ?") || word.equals("NEQ?")){
            tokens.add(new Token(TokenType.NUM_PRE, word));
        } else if(word.equals("INTER") || word.equals("UNION") || word.equals("NON")){
            tokens.add(new Token(TokenType.LOG_OPS, word));
        } else if(word.equals("IS") || word.equals("INTO")){
            tokens.add(new Token(TokenType.ASSIGN, word));
        } else if(word.equals("FLOAT") || word.equals("STRING") || word.equals("BOOL")){
            tokens.add(new Token(TokenType.D_TYPE, word));
        } else if(word.equals("YEAH") || word.equals("NOPE")){
            tokens.add(new Token(TokenType.BOOL, word));
        } else { 
            tokens.add(new Token(TokenType.INVALID, word));  
            containsInvalidTokens = true;
        }
    }
    
    public ArrayList<Token> getTokens(){
        return tokens;
    }
    
    public ArrayList<Variable> getVariables(){
        return variables;
    }
    
    public ArrayList<String> getOutput(){
        return output;
    }
    
    public ArrayList<String> getBegVariables(){
        return begVariables;
    }
    
    public String getParseErrorLocation(){
        return parseErrorLocation;
    }
    
    public boolean getContainsInvalidTokens(){
        return containsInvalidTokens;
    }
    
    public boolean getContainsParseError(){
        return containsParseError;
    }        
    
    public boolean getContainsBegVariables(){
        return containsBegVariables;
    }
    
    public static void addVariable(Variable v){
        variables.add(v);
    }
        
    
    public static void callParser(){
        parser();
    }
}
